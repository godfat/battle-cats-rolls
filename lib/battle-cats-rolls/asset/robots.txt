User-agent: *
Disallow: /*.png

User-agent: ClaudeBot
User-agent: GPTBot
User-agent: Facebot
User-agent: DataForSeoBot
User-agent: DotBot
User-agent: bingbot
User-agent: PetalBot
Disallow: /
Allow: /cats
Allow: /help
Allow: /logs
Allow: /seek
